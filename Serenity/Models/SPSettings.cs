﻿using System.ComponentModel;

namespace Serenity.Models
{
    public class SPSettings : INotifyPropertyChanged
    {
        private string discordUser, lastFMUser, lastPath, lastPlayed;
        private int random;

        public event PropertyChangedEventHandler PropertyChanged;

        public string DiscordUser
        {
            get { return discordUser; }
            set
            {
                if (discordUser == value) return;
                discordUser = value;
                NotifyPropertyChanged("DiscordUser");
            }
        }

        public string LastFMUser
        {
            get { return lastFMUser; }
            set
            {
                if (lastFMUser == value) return;
                lastFMUser = value;
                NotifyPropertyChanged("LastFMUser");
            }
        }

        public string LastPath
        {
            get { return lastPath; }
            set
            {
                if (lastPath == value) return;
                lastPath = value;
            }
        }

        public string LastPlayed
        {
            get { return lastPlayed; }
            set
            {
                if (lastPlayed == value) return;
                lastPlayed = value;
            }
        }

        public int Random
        {
            get { return random; }
            set
            {
                if (random == value) return;
                random = value;
                NotifyPropertyChanged("Random");
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
