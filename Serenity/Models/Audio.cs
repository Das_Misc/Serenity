﻿using System;
using System.ComponentModel;

namespace Serenity.Models
{
    public class Audio : INotifyPropertyChanged
    {
        #region Fields

        private string albumArtist, albumName, filename, trackArtist, trackName;
        private TimeSpan duration;
        private bool hasPlayed, isValid;
        private uint trackNumber;

        #endregion Fields

        #region Properties

        public string AlbumArtist
        {
            get { return albumArtist; }
            set
            {
                if (albumArtist == value) return;
                albumArtist = value;
                NotifyPropertyChanged("AlbumArtist");
            }
        }

        public string AlbumName
        {
            get { return albumName; }
            set
            {
                if (albumName == value) return;
                albumName = value;
                NotifyPropertyChanged("AlbumName");
            }
        }

        public TimeSpan Duration
        {
            get { return duration; }
            set
            {
                if (duration == value) return;
                duration = value;
                NotifyPropertyChanged("Duration");
            }
        }

        public string Filename
        {
            get { return filename; }
            set
            {
                if (filename == value) return;
                filename = value;
                NotifyPropertyChanged("Filename");
            }
        }

        public bool HasPlayed
        {
            get { return hasPlayed; }
            set
            {
                if (hasPlayed == value) return;
                hasPlayed = value;
                NotifyPropertyChanged("HasPlayed");
            }
        }

        public bool IsValid
        {
            get { return isValid; }
            set
            {
                if (isValid == value) return;
                isValid = value;
                NotifyPropertyChanged("IsValid");
            }
        }

        public string TrackArtist
        {
            get { return trackArtist; }
            set
            {
                if (trackArtist == value) return;
                trackArtist = value;
                NotifyPropertyChanged("TrackArtist");
            }
        }

        public string TrackName
        {
            get { return trackName; }
            set
            {
                if (trackName == value) return;
                trackName = value;
                NotifyPropertyChanged("TrackName");
            }
        }

        public uint TrackNumber
        {
            get { return trackNumber; }
            set
            {
                if (trackNumber == value) return;
                trackNumber = value;
                NotifyPropertyChanged("TrackNumber");
            }
        }

        #endregion Properties

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
