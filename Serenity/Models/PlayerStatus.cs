﻿using System;
using System.ComponentModel;
using System.Windows.Media;

namespace Serenity.Models
{
    public class PlayerStatus : INotifyPropertyChanged
    {
        #region Fields

        private Audio currentSong;
        private TimeSpan currentSongTime, totalDuration;
        private ImageSource image;
        private bool isPaused, isPlaying, isPausedOrPlaying, isPlayListPopulated, isStopped;
        private int random;

        #endregion Fields

        #region Properties

        public Audio CurrentSong
        {
            get => currentSong;
            set
            {
                if (currentSong == value) return;
                currentSong = value;
                NotifyPropertyChanged("CurrentSong");
            }
        }

        public TimeSpan CurrentSongTime
        {
            get => currentSongTime;
            set
            {
                if (currentSongTime == value) return;
                currentSongTime = value;
                NotifyPropertyChanged("CurrentSongTime");
            }
        }

        public ImageSource Image
        {
            get => image;
            set
            {
                if (image == value) return;
                image = value;
                NotifyPropertyChanged("Image");
            }
        }

        public bool IsPaused
        {
            get => isPaused;
            set
            {
                if (isPaused == value) return;
                isPaused = value;
                NotifyPropertyChanged("IsPaused");
                NotifyPropertyChanged("IsPausedOrPlaying");
            }
        }

        public bool IsPausedOrPlaying => IsPaused || IsPlaying;

        public bool IsPlaying
        {
            get => isPlaying;
            set
            {
                if (isPlaying == value) return;
                isPlaying = value;
                NotifyPropertyChanged("IsPlaying");
                NotifyPropertyChanged("IsPausedOrPlaying");
            }
        }

        public bool IsPlayListPopulated
        {
            get => isPlayListPopulated;
            set
            {
                if (isPlayListPopulated == value) return;
                isPlayListPopulated = value;
                NotifyPropertyChanged("IsPlayListPopulated");
            }
        }

        public bool IsStopped
        {
            get => isStopped;
            set
            {
                if (isStopped == value) return;
                isStopped = value;
                NotifyPropertyChanged("IsStopped");
            }
        }

        public int Random
        {
            get => random;
            set
            {
                if (random == value) return;
                random = value;
                NotifyPropertyChanged("Random");
            }
        }

        public TimeSpan TotalDuration
        {
            get => totalDuration;
            set
            {
                if (totalDuration == value) return;
                totalDuration = value;
                NotifyPropertyChanged("TotalDuration");
            }
        }

        #endregion Properties

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
