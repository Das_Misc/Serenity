﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Serenity.Converters
{
    internal class RandomToBrushesConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length == 2 && values[0] is int && values[1] is bool)
            {
                if ((int)values[0] == 2 && (bool)values[1])
                    return Brushes.LightYellow;
                else
                    return Brushes.LightGray;
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
