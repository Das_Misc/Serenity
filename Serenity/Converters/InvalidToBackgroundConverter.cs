﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Serenity.Converters
{
    internal class InvalidToBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                if (!(bool)value)
                    return Brushes.LightPink;
                else
                    return Brushes.Transparent;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
