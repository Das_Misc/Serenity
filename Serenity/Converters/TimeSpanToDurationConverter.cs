﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Serenity.Converters
{
    internal class TimeSpanToDurationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TimeSpan)
            {
                if (((TimeSpan)value).TotalHours >= 1)
                    return string.Format("{0:0}:{1:00}:{2:00}", ((TimeSpan)value).TotalHours, ((TimeSpan)value).Minutes, ((TimeSpan)value).Seconds);
                else
                    return string.Format("{0:0}:{1:00}", ((TimeSpan)value).Minutes, ((TimeSpan)value).Seconds);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
