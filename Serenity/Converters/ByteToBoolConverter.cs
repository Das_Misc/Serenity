﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Serenity.Converters
{
    internal class ByteToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int)
            {
                if ((int)value == 0)
                    return null;
                else if ((int)value == 1)
                    return false;
                else
                    return true;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                if (!(bool)value)
                    return 1;
                else
                    return 2;
            }
            else if (value == null)
                return 0;

            return null;
        }
    }
}
