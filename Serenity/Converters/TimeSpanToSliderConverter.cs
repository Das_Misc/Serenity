﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Serenity.Converters
{
    internal class TimeSpanToSliderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TimeSpan)
                return ((TimeSpan)value).TotalSeconds;

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double)
                return TimeSpan.FromSeconds((double)value);

            return null;
        }
    }
}
