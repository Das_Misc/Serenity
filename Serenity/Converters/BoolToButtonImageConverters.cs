﻿using Serenity.Views;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Serenity.Converters
{
    internal class ClearPlayListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();

                bi.UriSource = (bool)value ? new Uri("pack://application:,,,/Resources/Default/BlackPlayListClear.png") : new Uri("pack://application:,,,/Resources/Default/GreyPlayListClear.png");

                bi.EndInit();

                return bi;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    internal class NextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();

                bi.UriSource = (bool)value ? new Uri("pack://application:,,,/Resources/Default/BlackNext.png") : new Uri("pack://application:,,,/Resources/Default/GreyNext.png");

                bi.EndInit();

                return bi;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    internal class PauseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool && targetType == typeof(ImageSource))
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();

                bi.UriSource = (bool)value ? new Uri("pack://application:,,,/Resources/Default/BlackPause.png") : new Uri("pack://application:,,,/Resources/Default/GreyPause.png");

                bi.EndInit();

                return bi;
            }

            if (value is bool && targetType == typeof(Visibility))
                return (bool)value ? Visibility.Visible : Visibility.Collapsed;

            if (value is bool && targetType == typeof(bool))
                return (bool)value;

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    internal class PlayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool && targetType == typeof(ImageSource))
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();

                bi.UriSource = (bool)value ? new Uri("pack://application:,,,/Resources/Default/GreyPlay.png") : new Uri("pack://application:,,,/Resources/Default/BlackPlay.png");

                bi.EndInit();

                return bi;
            }

            if (value is bool && targetType == typeof(Visibility))
                return (bool)value ? Visibility.Collapsed : Visibility.Visible;

            if (value is bool && targetType == typeof(bool))
                return !(bool)value;

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    internal class PreviousConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();

                bi.UriSource = (bool)value ? new Uri("pack://application:,,,/Resources/Default/BlackPrevious.png") : new Uri("pack://application:,,,/Resources/Default/GreyPrevious.png");

                bi.EndInit();

                return bi;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    internal class RandomConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();

            if (value is int)
            {
                if ((int)value == 0)
                    bi.UriSource = new Uri("pack://application:,,,/Resources/Default/GreyRandom.png");
                else if ((int)value == 1)
                    bi.UriSource = new Uri("pack://application:,,,/Resources/Default/BlackRandom.png");
                else
                    bi.UriSource = new Uri("pack://application:,,,/Resources/Default/BlackRandom_iPod.png");
            }

            bi.EndInit();

            return bi;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    internal class StopConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();

                if (MainWindow.player != null && (MainWindow.PlayerStatus.IsPlaying || MainWindow.PlayerStatus.IsPaused))
                    bi.UriSource = new Uri("pack://application:,,,/Resources/Default/BlackStop.png");
                else
                    bi.UriSource = new Uri("pack://application:,,,/Resources/Default/GreyStop.png");

                bi.EndInit();

                return bi;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    internal class SubtractFromPlayListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();

                bi.UriSource = (bool)value ? new Uri("pack://application:,,,/Resources/Default/BlackPlayListRemove.png") : new Uri("pack://application:,,,/Resources/Default/GreyPlayListRemove.png");

                bi.EndInit();

                return bi;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
