﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;

namespace Serenity.Converters
{
    internal class FullPathToFilenameNoExtConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
                return Path.GetFileNameWithoutExtension(value as string);

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
