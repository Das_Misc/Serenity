﻿using Extra.Utilities;
using Serenity.Models;
using Serenity.Views;
using System;
using System.IO;
using System.Windows;
using System.Windows.Threading;

namespace Serenity
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        internal static SettingsFile SettingsFile;
        internal static SPSettings SPSettings = new SPSettings();

        private static MainWindow mainWindow;

        #region Entry point

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            InitializeApplication();

            if (SingleInstanceEnforcer.IsFirst(new SingleInstanceEnforcer.CommandLineDelegate(ArgumentsReceived)))
            {
                ReadSettings();

                mainWindow = e.Args.Length >= 1 ? new MainWindow(e.Args[0]) : new MainWindow(null);

                Current.MainWindow = mainWindow;

                mainWindow.Show();
            }
            else
            {
                if (e.Args.Length >= 1)
                    SingleInstanceEnforcer.PassCommandLine(e.Args);

                Environment.Exit(0);
            }
        }

        private void ArgumentsReceived(string[] arguments)
        {
            if (mainWindow == null)
                return;

            Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
            {
                if (arguments != null && arguments.Length > 0)
                    mainWindow.AddAudio(arguments[0]);
            }));
        }

        private void InitializeApplication()
        {
            string localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            string dataDirectory = Path.Combine(localAppData, Serenity.Properties.Resources.AppName);

            if (!Directory.Exists(dataDirectory))
                Directory.CreateDirectory(dataDirectory);

            SettingsFile = new SettingsFile(Path.Combine(dataDirectory, "settings.ini"));
        }

        #endregion Entry point

        #region Application settings

        internal static void WriteSettings(MainWindow mw)
        {
            SettingsFile.WriteSetting("Window", "Height", mw.Height);
            SettingsFile.WriteSetting("Window", "Left", mw.Left);
            SettingsFile.WriteSetting("Window", "Maximized", mw.WindowState == WindowState.Maximized ? true : false);
            SettingsFile.WriteSetting("Window", "Top", mw.Top);
            SettingsFile.WriteSetting("Window", "Width", mw.Width);

            SettingsFile.WriteSetting("Reproduction", "Last path", SPSettings.LastPath);
            SettingsFile.WriteSetting("Reproduction", "Last played", SPSettings.LastPlayed);
            SettingsFile.WriteSetting("Reproduction", "Random", Views.MainWindow.PlayerStatus.Random);

            SettingsFile.WriteSetting("LastFM", "User", SPSettings.LastFMUser);

            SettingsFile.WriteSetting("Discord", "User", SPSettings.DiscordUser);
        }

        private static void ReadSettings()
        {
            SPSettings.DiscordUser = SettingsFile.ReadSetting("Discord", "User", string.Empty);

            SPSettings.LastFMUser = SettingsFile.ReadSetting("LastFM", "User", string.Empty);

            SPSettings.LastPath = SettingsFile.ReadSetting("Reproduction", "Last path", string.Empty);
            SPSettings.LastPlayed = SettingsFile.ReadSetting("Reproduction", "Last played", string.Empty);
            SPSettings.Random = SettingsFile.ReadSetting("Reproduction", "Random", 0);
        }

        #endregion Application settings
    }
}
