﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Extra.Utilities
{
    internal class SettingsFile
    {
        #region Fields

        private string settingsFile;

        #endregion Fields

        #region Constructors

        internal SettingsFile() : this("settings.ini")
        {
        }

        internal SettingsFile(string settingsFile)
        {
            this.settingsFile = settingsFile;

            if (!File.Exists(settingsFile))
                using (File.Create(settingsFile)) { }
        }

        #endregion Constructors

        #region Internal Methods

        internal T ReadSetting<T>(string section, string setting, T defaultValue)
        {
            section = "[" + section + "]";

            string[] settings = File.ReadLines(settingsFile).ToArray();

            var sectionBlock = ExtractSectionBlock(section, settings);

            T value = ReadValue(sectionBlock, setting, defaultValue);

            return value;
        }

        internal void WriteSetting<T>(string section, string setting, T value)
        {
            section = "[" + section + "]";

            string[] settings = File.ReadLines(settingsFile).ToArray();

            var sectionBlock = ExtractSectionBlock(section, settings);

            var newSectionBlock = WriteValue(sectionBlock, section, setting, value);

            string[] newSettings = UpdateSettings(settings, newSectionBlock, sectionBlock.Count);

            File.WriteAllLines(settingsFile, newSettings);
        }

        #endregion Internal Methods

        #region Private Methods

        private List<string> ExtractSectionBlock(string section, string[] settings)
        {
            var sectionBlock = new List<string>();

            bool sectionFound = false;

            for (int i = 0; i < settings.Length; i++)
            {
                if (settings[i] != section && !sectionFound)
                    continue;

                if (settings[i] != section && settings[i].StartsWith("[") && settings[i].EndsWith("]"))
                    break;

                sectionFound = true;
                sectionBlock.Add(settings[i]);
            }

            return sectionBlock;
        }

        private T ReadValue<T>(List<string> sectionBlock, string setting, T defaultValue)
        {
            string value = null;

            foreach (var settingLine in sectionBlock)
                if (settingLine.Contains(setting) && !settingLine.StartsWith("[") && !settingLine.EndsWith("]"))
                    value = settingLine.Substring(setting.Length + 1);

            if (string.IsNullOrWhiteSpace(value))
                return defaultValue;

            try
            {
                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch (FormatException)
            {
                return defaultValue;
            }
        }

        private string[] UpdateSettings(string[] settings, List<string> newSectionBlock, int blockSizeToRemove)
        {
            var settingsList = settings.ToList();

            int index = 0;

            bool sectionFound = false;

            for (int i = 0; i < settingsList.Count; i++)
            {
                if (settingsList[i] != newSectionBlock[0] && !sectionFound)
                    continue;

                if (settingsList[i] == newSectionBlock[0])
                    index = i;

                sectionFound = true;

                if (settingsList[i] != newSectionBlock[0] && settingsList[i].StartsWith("[") && settingsList[i].EndsWith("]"))
                    break;
            }

            if (sectionFound)
                settingsList.RemoveRange(index, blockSizeToRemove);

            settingsList.InsertRange(index, newSectionBlock);

            return settingsList.ToArray();
        }

        private List<string> WriteValue<T>(List<string> sectionBlock, string section, string setting, T value)
        {
            string[] tmpSectionBlockArr = new string[sectionBlock.Count];

            sectionBlock.CopyTo(tmpSectionBlockArr);

            var tmpSectionBlock = tmpSectionBlockArr.ToList();

            if (sectionBlock.Count == 0)
            {
                tmpSectionBlock.Add(section);
                tmpSectionBlock.Add(setting + "=" + value.ToString());

                return tmpSectionBlock;
            }

            if (!sectionBlock.Any(x => x.Contains(setting) && !x.StartsWith("[") && !x.EndsWith("]")))
                tmpSectionBlock.Add(setting + "=" + value.ToString());
            else
                for (int i = 0; i < sectionBlock.Count; i++)
                    if (sectionBlock[i].Contains(setting) && !sectionBlock[i].StartsWith("[") && !sectionBlock[i].EndsWith("]"))
                        tmpSectionBlock[i] = setting + "=" + value.ToString();

            return tmpSectionBlock;
        }

        #endregion Private Methods
    }
}
