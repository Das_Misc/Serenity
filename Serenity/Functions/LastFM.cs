﻿using Extra.Utilities;
using Newtonsoft.Json.Linq;
using Serenity.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Serenity.Functions
{
    internal class LastFM
    {
        internal const string apiKey = "0c9cca6460b4075c580eb092605bb753";
        internal static Dictionary<string, string> session;
        internal static string token;
        private const string rootURL = "http://ws.audioscrobbler.com/2.0/";
        private const string secret = "6ecd6b628376b548d48330d177b6793e";
        private static readonly string userAgent = Properties.Resources.AppName + " " + Assembly.GetExecutingAssembly().GetName().Version;

        internal static async void GetRequestToken()
        {
            await Task.Run(() =>
            {
                HttpWebRequest dataRequest = (HttpWebRequest)WebRequest.Create(rootURL + "?method=auth.getToken");
                dataRequest.Method = "POST";
                dataRequest.ContentType = "application/x-www-form-urlencoded";
                dataRequest.UserAgent = userAgent;

                string apiSignature = GetAPISig("auth.getToken", false);

                StringBuilder postDataStr = new StringBuilder();
                postDataStr.Append("api_key=" + HttpUtility.UrlEncode(apiKey, Encoding.UTF8) + "&");
                postDataStr.Append("api_sig=" + HttpUtility.UrlEncode(apiSignature, Encoding.UTF8) + "&");
                postDataStr.Append("format=" + HttpUtility.UrlEncode("json", Encoding.UTF8));
                byte[] postData = Encoding.UTF8.GetBytes(postDataStr.ToString());

                using (Stream post = dataRequest.GetRequestStream())
                using (BinaryWriter bw = new BinaryWriter(post))
                    bw.Write(postData);

                using (WebResponse dump = dataRequest.GetResponse())
                using (Stream dumpData = dump.GetResponseStream())
                using (var sr = new StreamReader(dumpData))
                {
                    string response = sr.ReadToEnd();

                    JToken json = JObject.Parse(response);

                    token = json["token"].ToString();
                }

                GetAuthorization();
            });
        }

        internal static void GetSession()
        {
            HttpWebRequest dataRequest = (HttpWebRequest)WebRequest.Create(rootURL + "?method=auth.getSession");
            dataRequest.Method = "POST";
            dataRequest.ContentType = "application/x-www-form-urlencoded";
            dataRequest.UserAgent = userAgent;

            string apiSignature = GetAPISig("auth.getSession");

            StringBuilder postDataStr = new StringBuilder();
            postDataStr.Append("api_key=" + HttpUtility.UrlEncode(apiKey, Encoding.UTF8) + "&");
            postDataStr.Append("api_sig=" + HttpUtility.UrlEncode(apiSignature, Encoding.UTF8) + "&");
            postDataStr.Append("token=" + HttpUtility.UrlEncode(token, Encoding.UTF8) + "&");
            postDataStr.Append("format=" + HttpUtility.UrlEncode("json", Encoding.UTF8));
            byte[] postData = Encoding.UTF8.GetBytes(postDataStr.ToString());
            dataRequest.ContentLength = postData.Length;

            using (Stream post = dataRequest.GetRequestStream())
                post.Write(postData, 0, postData.Length);

            using (WebResponse dump = dataRequest.GetResponse())
            using (Stream dumpData = dump.GetResponseStream())
            using (var sr = new StreamReader(dumpData))
            {
                string response = sr.ReadToEnd();

                JToken json = JObject.Parse(response);

                var sessionInformation = new Dictionary<string, string>();

                sessionInformation.Add("name", json["session"]["name"].ToString());
                sessionInformation.Add("key", json["session"]["key"].ToString());

                session = sessionInformation;
            }
        }

        internal static void LoadSession(string sessionFile)
        {
            session = new Dictionary<string, string>();

            session.Add("name", App.SPSettings.LastFMUser);

            string encryptedKey = File.ReadAllText(sessionFile);

            string decryptedKey = CryptographicEngines.Decrypt(encryptedKey, apiKey, Encoding.UTF8.GetBytes(secret), CipherMode.CBC, PaddingMode.None);

            session.Add("key", decryptedKey);
        }

        internal static void SaveSession(string sessionFile)
        {
            using (FileStream fs = File.Create(sessionFile))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                string encryptedKey = CryptographicEngines.Encrypt(session["key"], apiKey, Encoding.UTF8.GetBytes(secret), CipherMode.CBC, PaddingMode.None);

                sw.Write(encryptedKey);
            }
        }

        internal static async void Scrobble(Audio audio)
        {
            await Task.Run(() =>
            {
                HttpWebRequest dataRequest = (HttpWebRequest)WebRequest.Create(rootURL + "?method=track.scrobble");
                dataRequest.Method = "POST";
                dataRequest.ContentType = "application/x-www-form-urlencoded";
                dataRequest.UserAgent = userAgent;

                string apiSignature;

                using (var hash = MD5.Create())
                {
                    string apiSignatureStringRaw = "album" + audio.AlbumName + "albumArtist" + audio.AlbumArtist + "api_key" + apiKey + "artist" + audio.TrackArtist +
                                                   "duration" + ((int)audio.Duration.TotalSeconds).ToString() + "methodtrack.scrobble" + "sk" + session["key"] +
                                                   "timestamp" + ((int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds).ToString() +
                                                   "track" + audio.TrackName + "trackNumber" + audio.TrackNumber + secret;

                    byte[] apiSignatureRaw = Encoding.UTF8.GetBytes(apiSignatureStringRaw);

                    byte[] apiSignatureArr = hash.ComputeHash(apiSignatureRaw);

                    StringBuilder sb = new StringBuilder();

                    for (int i = 0; i < apiSignatureArr.Length; i++)
                        sb.Append(apiSignatureArr[i].ToString("x2"));

                    apiSignature = sb.ToString();
                }

                StringBuilder postDataStr = new StringBuilder();
                postDataStr.Append("albumArtist=" + HttpUtility.UrlEncode(audio.AlbumArtist, Encoding.UTF8) + "&");
                postDataStr.Append("album=" + HttpUtility.UrlEncode(audio.AlbumName, Encoding.UTF8) + "&");
                postDataStr.Append("api_key=" + HttpUtility.UrlEncode(apiKey, Encoding.UTF8) + "&");
                postDataStr.Append("api_sig=" + HttpUtility.UrlEncode(apiSignature, Encoding.UTF8) + "&");
                postDataStr.Append("artist=" + HttpUtility.UrlEncode(audio.TrackArtist, Encoding.UTF8) + "&");
                postDataStr.Append("duration=" + HttpUtility.UrlEncode(((int)audio.Duration.TotalSeconds).ToString(), Encoding.UTF8) + "&");
                postDataStr.Append("timestamp=" + HttpUtility.UrlEncode(((int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds).ToString(), Encoding.UTF8) + "&");
                postDataStr.Append("trackNumber=" + HttpUtility.UrlEncode(audio.TrackNumber.ToString(), Encoding.UTF8) + "&");
                postDataStr.Append("track=" + HttpUtility.UrlEncode(audio.TrackName, Encoding.UTF8) + "&");
                postDataStr.Append("sk=" + HttpUtility.UrlEncode(session["key"], Encoding.UTF8) + "&");
                postDataStr.Append("format=" + HttpUtility.UrlEncode("json", Encoding.UTF8));
                byte[] postData = Encoding.UTF8.GetBytes(postDataStr.ToString());
                dataRequest.ContentLength = postData.Length;

                try
                {
                    using (Stream post = dataRequest.GetRequestStream())
                        post.Write(postData, 0, postData.Length);

                    using (WebResponse dump = dataRequest.GetResponse())
                    using (Stream dumpData = dump.GetResponseStream())
                    using (var sr = new StreamReader(dumpData))
                    {
                        string response = sr.ReadToEnd();

                        JToken json = JObject.Parse(response);
                    }
                }
                catch (WebException) { }

                UpdateNowPlaying(audio);
            });
        }

        private static string GetAPISig(string method, bool requiresAuth = true)
        {
            using (var hash = MD5.Create())
            {
                string apiSignatureString;

                if (requiresAuth)
                    apiSignatureString = "api_key" + apiKey + "method" + method + "token" + token + secret;
                else
                    apiSignatureString = "api_key" + apiKey + method;

                byte[] apiSignatureRaw = Encoding.UTF8.GetBytes(apiSignatureString);

                byte[] apiSignatureArr = hash.ComputeHash(apiSignatureRaw);

                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < apiSignatureArr.Length; i++)
                    sb.Append(apiSignatureArr[i].ToString("x2"));

                return sb.ToString();
            }
        }

        private static void GetAuthorization()
        {
            Process.Start("http://www.last.fm/api/auth/?api_key=" + apiKey + "&token=" + token);
        }

        private static void UpdateNowPlaying(Audio audio)
        {
            HttpWebRequest dataRequest = (HttpWebRequest)WebRequest.Create(rootURL + "?method=track.updateNowPlaying");
            dataRequest.Method = "POST";
            dataRequest.ContentType = "application/x-www-form-urlencoded";
            dataRequest.UserAgent = userAgent;

            string apiSignature;

            using (var hash = MD5.Create())
            {
                string apiSignatureStringRaw = "album" + audio.AlbumName + "albumArtist" + audio.AlbumArtist + "api_key" + apiKey + "artist" + audio.TrackArtist +
                                               "duration" + ((int)audio.Duration.TotalSeconds).ToString() + "methodtrack.updateNowPlaying" + "sk" + session["key"] +
                                               "track" + audio.TrackName + "trackNumber" + audio.TrackNumber + secret;

                byte[] apiSignatureRaw = Encoding.UTF8.GetBytes(apiSignatureStringRaw);

                byte[] apiSignatureArr = hash.ComputeHash(apiSignatureRaw);

                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < apiSignatureArr.Length; i++)
                    sb.Append(apiSignatureArr[i].ToString("x2"));

                apiSignature = sb.ToString();
            }

            StringBuilder postDataStr = new StringBuilder();
            postDataStr.Append("albumArtist=" + HttpUtility.UrlEncode(audio.AlbumArtist, Encoding.UTF8) + "&");
            postDataStr.Append("album=" + HttpUtility.UrlEncode(audio.AlbumName, Encoding.UTF8) + "&");
            postDataStr.Append("api_key=" + HttpUtility.UrlEncode(apiKey, Encoding.UTF8) + "&");
            postDataStr.Append("api_sig=" + HttpUtility.UrlEncode(apiSignature, Encoding.UTF8) + "&");
            postDataStr.Append("artist=" + HttpUtility.UrlEncode(audio.TrackArtist, Encoding.UTF8) + "&");
            postDataStr.Append("duration=" + HttpUtility.UrlEncode(((int)audio.Duration.TotalSeconds).ToString(), Encoding.UTF8) + "&");
            postDataStr.Append("trackNumber=" + HttpUtility.UrlEncode(audio.TrackNumber.ToString(), Encoding.UTF8) + "&");
            postDataStr.Append("track=" + HttpUtility.UrlEncode(audio.TrackName, Encoding.UTF8) + "&");
            postDataStr.Append("sk=" + HttpUtility.UrlEncode(session["key"], Encoding.UTF8) + "&");
            postDataStr.Append("format=" + HttpUtility.UrlEncode("json", Encoding.UTF8));
            byte[] postData = Encoding.UTF8.GetBytes(postDataStr.ToString());
            dataRequest.ContentLength = postData.Length;

            try
            {
                using (Stream post = dataRequest.GetRequestStream())
                    post.Write(postData, 0, postData.Length);

                using (WebResponse dump = dataRequest.GetResponse())
                using (Stream dumpData = dump.GetResponseStream())
                using (var sr = new StreamReader(dumpData))
                {
                    string response = sr.ReadToEnd();

                    JToken json = JObject.Parse(response);
                }
            }
            catch (WebException) { }
        }
    }
}
