﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Serenity.Functions
{
    internal class Discord
    {
        private static readonly string BaseURL = "https://discord.com/api/v9";
        private static string token;

        internal static void ClearStatus()
        {
            if (string.IsNullOrWhiteSpace(token))
                return;

            Task.Run(() =>
            {
                HttpWebRequest dataRequest = (HttpWebRequest)WebRequest.Create(BaseURL + "/users/@me/settings");
                dataRequest.Method = "PATCH";
                dataRequest.Host = "discord.com";
                dataRequest.Referer = "https://discord.com/channels/@me";
                dataRequest.ContentType = "application/json";
                dataRequest.Headers.Add("Origin", "https://discord.com");
                dataRequest.Headers.Add("Authorization", token);

                JObject json = new JObject(new JProperty("custom_status", null));
                byte[] postData = Encoding.UTF8.GetBytes(json.ToString());

                using (Stream post = dataRequest.GetRequestStream())
                using (BinaryWriter bw = new BinaryWriter(post))
                    bw.Write(postData);

                using (HttpWebResponse response = (HttpWebResponse)dataRequest.GetResponse())
                    return;
            });
        }

        internal static void SignIn(string username, string password, string dataDirectory)
        {
            HttpWebRequest dataRequest = (HttpWebRequest)WebRequest.Create(BaseURL + "/auth/login");
            dataRequest.Method = "POST";
            dataRequest.Host = "discord.com";
            dataRequest.Referer = "https://discord.com/login";
            dataRequest.ContentType = "application/json";
            dataRequest.Headers.Add("Origin", "https://discord.com");

            JObject json = new JObject(new JProperty("login", username), new JProperty("password", password), new JProperty("undelete", false), new JProperty("captcha_key", null), new JProperty("login_source", null), new JProperty("gift_code_sku_id", null));
            byte[] postData = Encoding.UTF8.GetBytes(json.ToString());

            using (Stream post = dataRequest.GetRequestStream())
            using (BinaryWriter bw = new BinaryWriter(post))
                bw.Write(postData);

            try
            {
                using (WebResponse dump = dataRequest.GetResponse())
                using (Stream dumpData = dump.GetResponseStream())
                using (var sr = new StreamReader(dumpData))
                {
                    string response = sr.ReadToEnd();

                    token = JObject.Parse(response)["token"].ToString();

                    App.SPSettings.DiscordUser = username;
                    File.WriteAllText(Path.Combine(dataDirectory, App.SPSettings.DiscordUser), token);
                }
            }
            catch (WebException) { MessageBox.Show(Properties.Resources.InvalidCredentials); }
        }

        internal static void SignIn(string tokenFile)
        {
            token = File.ReadAllText(tokenFile);
        }

        internal static void SignOut(string tokenFile)
        {
            Task.Run(() =>
            {
                HttpWebRequest dataRequest = (HttpWebRequest)WebRequest.Create(BaseURL + "/auth/logout");
                dataRequest.Method = "POST";
                dataRequest.Host = "discord.com";
                dataRequest.Referer = "https://discord.com/login";
                dataRequest.ContentType = "application/json";
                dataRequest.Headers.Add("Origin", "https://discord.com");
                dataRequest.Headers.Add("Authorization", token);

                JObject json = new JObject(new JProperty("provider", null), new JProperty("voip_provider", null));
                byte[] postData = Encoding.UTF8.GetBytes(json.ToString());

                using (Stream post = dataRequest.GetRequestStream())
                using (BinaryWriter bw = new BinaryWriter(post))
                    bw.Write(postData);

                using (HttpWebResponse dump = (HttpWebResponse)dataRequest.GetResponse())
                    if (dump.StatusCode == HttpStatusCode.NoContent)
                        File.Delete(tokenFile);
            });
        }

        internal static void UpdateStatus(string status, int length)
        {
            if (string.IsNullOrWhiteSpace(token))
                return;

            Task.Run(() =>
            {
                HttpWebRequest dataRequest = (HttpWebRequest)WebRequest.Create(BaseURL + "/users/@me/settings");
                dataRequest.Method = "PATCH";
                dataRequest.Host = "discord.com";
                dataRequest.Referer = "https://discord.com/login";
                dataRequest.ContentType = "application/json";
                dataRequest.Headers.Add("Origin", "https://discord.com");
                dataRequest.Headers.Add("Authorization", token);

                string expirationTime = DateTime.UtcNow.AddSeconds(length).ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffZ");
                JObject json = new JObject(new JProperty("custom_status", new JObject(new JProperty("text", "Now Playing: " + status), new JProperty("expires_at", expirationTime))));
                byte[] postData = Encoding.UTF8.GetBytes(json.ToString());

                using (Stream post = dataRequest.GetRequestStream())
                using (BinaryWriter bw = new BinaryWriter(post))
                    bw.Write(postData);

                using (HttpWebResponse response = (HttpWebResponse)dataRequest.GetResponse())
                    return;
            });
        }
    }
}
