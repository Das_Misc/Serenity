﻿// Written by Josh Smith - 9/2007
using System;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Extra.Controls
{
    /// <summary>
    /// A Slider which provides a way to modify the
    /// auto tooltip text by using a format string.
    /// </summary>
    public class FormattedSlider : Slider
    {
        private ToolTip _autoToolTip;
        private string _autoToolTipFormat;

        /// <summary>
        /// Gets/sets a format string used to modify the auto tooltip's content.
        /// Note: This format string must contain exactly one placeholder value,
        /// which is used to hold the tooltip's original content.
        /// </summary>
        public string AutoToolTipFormat
        {
            get { return _autoToolTipFormat; }
            set { _autoToolTipFormat = value; }
        }

        private ToolTip AutoToolTip
        {
            get
            {
                if (_autoToolTip == null)
                {
                    FieldInfo field = typeof(Slider).GetField("_autoToolTip", BindingFlags.NonPublic | BindingFlags.Instance);

                    _autoToolTip = field.GetValue(this) as ToolTip;
                }

                return _autoToolTip;
            }
        }

        protected override void OnThumbDragDelta(DragDeltaEventArgs e)
        {
            base.OnThumbDragDelta(e);
            FormatAutoToolTipContent();
        }

        protected override void OnThumbDragStarted(DragStartedEventArgs e)
        {
            base.OnThumbDragStarted(e);
            FormatAutoToolTipContent();
        }

        private void FormatAutoToolTipContent()
        {
            if (string.IsNullOrEmpty(AutoToolTipFormat))
                return;

            object content;

            string text = AutoToolTip.Content as string;
            double number;

            if (double.TryParse(text, out number))
            {
                if (TimeSpan.FromSeconds(number).TotalHours >= 1)
                    content = TimeSpan.FromSeconds(number).ToString(@"h\:mm\:ss");
                else if (TimeSpan.FromSeconds(number).TotalMinutes >= 10)
                    content = TimeSpan.FromSeconds(number).ToString(@"mm\:ss");
                else
                    content = TimeSpan.FromSeconds(number).ToString(@"m\:ss");
            }
            else
                content = text;

            AutoToolTip.Content = string.Format(AutoToolTipFormat, content);
        }
    }
}
