﻿using Extra.Controls;
using Extra.Utilities;
using Mantin.Controls.Wpf.Notification;
using Microsoft.Win32;
using Serenity.Functions;
using Serenity.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shell;
using System.Windows.Threading;
using WpfScreenHelper;

namespace Serenity.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        #region Fields

        internal static MediaPlayer player = new MediaPlayer();

        private const int WM_NCLBUTTONDOWN = 0x00a1;
        private static bool dragStarted;
        private static string playlistFile;
        private static DispatcherTimer timer;

        // this is the offset of the mouse cursor from the top left corner of the window
        private Point offset;

        private readonly List<string> playedList = new List<string>();

        #endregion Fields

        #region Properties

        public static PlayerStatus PlayerStatus { get; private set; }
        public ObservableCollection<Audio> PlayList { get; }

        private ObservableCollection<Audio> PreSearchPlayList { get; set; }

        #endregion Properties

        #region Constructor

        public MainWindow(string starterSong)
        {
            CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, OnCloseWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand, OnMaximizeWindow, OnCanResizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, OnMinimizeWindow, OnCanMinimizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, OnRestoreWindow, OnCanResizeWindow));

            Style = FindResource("MyMainWindowStyle") as Style;

            PlayList = new ObservableCollection<Audio>();
            PreSearchPlayList = new ObservableCollection<Audio>();

            Loaded += (sender, e) => MainWindow_Loaded(sender, e, starterSong);

            PlayerStatus = new PlayerStatus();
            PlayerStatus.TotalDuration = TimeSpan.Zero;
            PlayerStatus.Random = App.SPSettings.Random;
            PlayerStatus.IsPlaying = false;
            PlayerStatus.IsPaused = false;
            PlayerStatus.IsStopped = true;
            SetAlbumArt();

            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += Timer_Tick;

            RegisterHotKeys();

            string localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            string dataDirectory = Path.Combine(localAppData, Properties.Resources.AppName);

            playlistFile = Path.Combine(dataDirectory, "playlist.bin");

            if (!string.IsNullOrEmpty(App.SPSettings.LastFMUser) && File.Exists(Path.Combine(dataDirectory, App.SPSettings.LastFMUser)))
                LastFM.LoadSession(Path.Combine(dataDirectory, App.SPSettings.LastFMUser));
            else
                App.SPSettings.LastFMUser = string.Empty;

            if (!string.IsNullOrEmpty(App.SPSettings.DiscordUser) && File.Exists(Path.Combine(dataDirectory, App.SPSettings.DiscordUser)))
                Discord.SignIn(Path.Combine(dataDirectory, App.SPSettings.DiscordUser));
            else
                App.SPSettings.DiscordUser = string.Empty;

            InitializeComponent();
        }

        #endregion Constructor

        #region CanExecute

        private void CheckUpdates_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = File.Exists("Updater.exe");
        }

        private void IsPlaying_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = PlayerStatus.IsPlaying;
        }

        private void IsPlayingOrPaused_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = player != null && (PlayerStatus.IsPlaying || PlayerStatus.IsPaused);
        }

        private void IsPlayListPopulated_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = PlayerStatus.IsPlayListPopulated = PlayList.Count > 0;
        }

        #endregion CanExecute

        #region Executed

        private void About_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Assembly thisAssembly = Assembly.GetExecutingAssembly();

            string message = string.Format("Serenity v{0}{1}by Dasanko{1}{1}The Serenity audio player is a tribute to serenitydiary, with love.{1}{1}{1}" +
                                           "Icons by Google Inc., under the Creative Commons Attribution-ShareAlike 3.0 Unported License: http://creativecommons.org/licenses/by-sa/3.0",
                                           thisAssembly.GetName().Version, Environment.NewLine);

            MessageBox.Show(message, Properties.Resources.AppName, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void AddAudio_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.CheckFileExists = false;
            ofd.FileName = Properties.Resources.AddFolder;
            ofd.Filter = Properties.Resources.SupportedAudioFormats;
            ofd.InitialDirectory = App.SPSettings.LastPath;
            ofd.Multiselect = true;

            if (ofd.ShowDialog().Value)
            {
                AddDirectoryToPlayList(ofd.FileName);

                AddFilesToPlayList(ofd.FileNames);

                if (File.Exists(ofd.FileName))
                    App.SPSettings.LastPath = Path.GetDirectoryName(ofd.FileName).Replace(".aac", "");
                else
                    App.SPSettings.LastPath = ofd.FileName.Replace(Properties.Resources.AddFolder, "").Replace(".aac", "");

                App.WriteSettings(this);
            }
        }

        private async void AddDirectoryToPlayList(string directory)
        {
            string dir = directory.Replace(Properties.Resources.AddFolder + ".aac", "");

            if (File.Exists(dir))
                return;

            await Task.Run(() =>
            {
                var fileNames = Directory.EnumerateFiles(dir, "*.*", SearchOption.AllDirectories).Where(
                    x => x.EndsWith(".aac", StringComparison.InvariantCultureIgnoreCase) || x.EndsWith("flac", StringComparison.InvariantCultureIgnoreCase) || x.EndsWith("m4a", StringComparison.InvariantCultureIgnoreCase) ||
                         x.EndsWith("mp3", StringComparison.InvariantCultureIgnoreCase) || x.EndsWith("ogg", StringComparison.InvariantCultureIgnoreCase) || x.EndsWith("wav", StringComparison.InvariantCultureIgnoreCase) ||
                         x.EndsWith("wma", StringComparison.InvariantCultureIgnoreCase));

                PlayerStatus.TotalDuration = TimeSpan.Zero;

                foreach (var fileName in fileNames)
                {
                    var audio = new Audio();
                    audio.Filename = fileName;
                    audio.HasPlayed = false;
                    audio.IsValid = true;

                    try
                    {
                        audio.Duration = TagLib.File.Create(audio.Filename).Properties.Duration;
                        
                        PlayerStatus.TotalDuration += audio.Duration;

                        Application.Current.Dispatcher.BeginInvoke((Action)delegate ()
                        {
                            PlayList.Add(audio);
                        });
                    }
                    catch (Exception e) when (e is TagLib.CorruptFileException || e is TagLib.UnsupportedFormatException || e is NullReferenceException) { }
                }

                Application.Current.Dispatcher.BeginInvoke((Action)delegate ()
                {
                    WriteDefaultPlaylist();
                });
            });
        }

        private async void AddFilesToPlayList(string[] fileNames)
        {
            if (fileNames.Length == 0 || string.IsNullOrWhiteSpace(fileNames[0]) || !File.Exists(fileNames[0]))
                return;

            await Task.Run(() =>
            {
                PlayerStatus.TotalDuration = TimeSpan.Zero;

                foreach (string fileName in fileNames)
                {
                    if (!fileName.EndsWith("aac", StringComparison.InvariantCultureIgnoreCase) && !fileName.EndsWith("flac", StringComparison.InvariantCultureIgnoreCase) &&
                        !fileName.EndsWith("m4a", StringComparison.InvariantCultureIgnoreCase) && !fileName.EndsWith("mp3", StringComparison.InvariantCultureIgnoreCase) &&
                        !fileName.EndsWith("ogg", StringComparison.InvariantCultureIgnoreCase) && !fileName.EndsWith("wav", StringComparison.InvariantCultureIgnoreCase) &&
                        !fileName.EndsWith("wma", StringComparison.InvariantCultureIgnoreCase))
                        continue;

                    var audio = new Audio();
                    audio.Filename = fileName;
                    audio.HasPlayed = false;
                    audio.IsValid = true;

                    try
                    {
                        audio.Duration = TagLib.File.Create(audio.Filename).Properties.Duration;
                        
                        PlayerStatus.TotalDuration += audio.Duration;

                        Application.Current.Dispatcher.BeginInvoke((Action)delegate() { PlayList.Add(audio); });
                    }
                    catch (Exception e) when (e is TagLib.CorruptFileException || e is TagLib.UnsupportedFormatException || e is NullReferenceException || e is FileNotFoundException) { }
                }

                Application.Current.Dispatcher.BeginInvoke((Action)delegate ()
                {
                    WriteDefaultPlaylist();
                });
            });
        }

        private void CheckUpdates_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();
            Process.Start("Updater.exe", $"\"{thisAssembly.GetName().Name}\" \"{thisAssembly.GetName().Version}\" \"{Process.GetCurrentProcess().MainModule.FileName}\"");
        }

        private void DataGridScrollToBottom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dg = sender as DataGrid;

            if (dg != null)
            {
                dg.SelectedItem = dg.Items.GetItemAt(dg.Items.Count - 1);

                dg.ScrollIntoView(dg.SelectedItem, dg.CurrentColumn);
            }
        }

        private void LoadPlaylist_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.Filter = Properties.Resources.SupportedPlaylistFormats;
            ofd.InitialDirectory = App.SPSettings.LastPath;
            ofd.Multiselect = false;

            if (ofd.ShowDialog().Value)
            {
                if (!string.IsNullOrWhiteSpace(searchTextBox.Text))
                    searchTextBox.Text = string.Empty;

                PlayList.Clear();

                AddFilesToPlayList(File.ReadAllLines(ofd.FileName));

                PreSearchPlayList = PlayList.Copy();

                App.SPSettings.LastPath = Path.GetDirectoryName(ofd.FileName);

                App.WriteSettings(this);

                WriteDefaultPlaylist();
            }
        }

        private bool LoadSong(Audio audio)
        {
            try
            {
                player.Open(new Uri(audio.Filename));
            }
            catch (DriveNotFoundException)
            {
                var toastPopUp = new ToastPopUp(Properties.Resources.AppName, string.Format(Properties.Resources.CorruptAudioNoPlay, Path.GetFileNameWithoutExtension(audio.Filename)), NotificationType.Warning);
                toastPopUp.Background = Brushes.WhiteSmoke;
                toastPopUp.Show();

                if (PlayList.Count == 1)
                    StopSong_Executed(null, null);
                else
                    PlayNextSong_Executed(null, null);

                return false;
            }
            catch (InvalidDataException)
            {
                var toastPopUp = new ToastPopUp(Properties.Resources.AppName, string.Format(Properties.Resources.CorruptAudioMissingData, Path.GetFileNameWithoutExtension(audio.Filename)), NotificationType.Warning);
                toastPopUp.Background = Brushes.WhiteSmoke;
                toastPopUp.Show();

                audio.IsValid = false;
            }

            App.SPSettings.LastPlayed = audio.Filename;

            playedList.Add(audio.Filename);

            if (PlayerStatus.Random == 2)
                audio.HasPlayed = true;

            return true;
        }

        private void LoadTagData(TagLib.File file)
        {
            string[] data = Path.GetFileNameWithoutExtension(file.Name).Split('-');

            if (file.Tag.AlbumArtists.Length > 0)
                PlayerStatus.CurrentSong.AlbumArtist = file.Tag.AlbumArtists[0];
            else
                PlayerStatus.CurrentSong.AlbumArtist = data[0];

            PlayerStatus.CurrentSong.AlbumName = file.Tag.Album;

            if (file.Tag.Performers.Length > 0)
                PlayerStatus.CurrentSong.TrackArtist = file.Tag.Performers[0];
            else
                PlayerStatus.CurrentSong.TrackArtist = data[0];

            if (!string.IsNullOrEmpty(file.Tag.Title))
                PlayerStatus.CurrentSong.TrackName = file.Tag.Title;
            else if (data.Length == 2)
                PlayerStatus.CurrentSong.TrackName = data[1].Trim();
            else
                PlayerStatus.CurrentSong.TrackName = data[0].Trim();

            PlayerStatus.CurrentSong.TrackNumber = file.Tag.Track;
        }

        private void OnHotkeyHandler(HotKey hotKey)
        {
            if (hotKey.Key == Key.MediaPlayPause && (player == null || PlayerStatus.IsPaused || PlayerStatus.IsStopped))
                PlaySong_Executed(null, null);
            else if (hotKey.Key == Key.MediaPlayPause && PlayerStatus.IsPlaying)
                PauseSong_Executed(null, null);
            else if (hotKey.Key == Key.MediaNextTrack)
                PlayNextSong_Executed(null, null);
            else if (hotKey.Key == Key.MediaPreviousTrack)
                PlayPreviousSong_Executed(null, null);
            else if (hotKey.Key == Key.MediaStop)
                StopSong_Executed(null, null);
        }

        private void OpenImageWindow_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var imageButton = e.OriginalSource as Button;
            var image = imageButton.Content as Image;

            if (image.Source == null)
                return;

            var imageWindow = new ImageWindow(image.Source);
            imageWindow.Closing += (theSender, args) => { imageWindow.Owner = null; };

            if (WindowState == WindowState.Maximized)
            {
                imageWindow.Top = (ActualHeight / 2) - (image.Height * 4.20);
                imageWindow.Left = (ActualWidth / 2) - (image.Width * 4);

                imageWindow.Height = 800;
                imageWindow.Width = 800;
            }
            else
            {
                imageWindow.Top = Top + (ActualHeight / 2) - (image.Height * 2);
                imageWindow.Left = Left + (ActualWidth / 2) - (image.Width * 2);

                imageWindow.Height = 390;
                imageWindow.Width = 400;
            }

            imageWindow.Show();
        }

        private void PauseSong_Executed(object sender, EventArgs e)
        {
            player.Pause();

            TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Paused;

            timer.IsEnabled = false;

            PlayerStatus.IsPlaying = false;
            PlayerStatus.IsPaused = true;
            PlayerStatus.IsStopped = false;
        }

        private void PlayNextSong_Executed(object sender, EventArgs e)
        {
            var dg = VisualTreeHelpers.FindChild<DataGrid>(this);

            if (PlayerStatus.Random == 0)
            {
                if (dg.SelectedIndex < PlayList.Count - 1)
                    dg.SelectedItem = dg.Items[dg.SelectedIndex + 1];
                else
                    dg.SelectedItem = dg.Items[0];
            }
            else if (PlayerStatus.Random == 1)
            {
                using (var rng = new RNGCryptoServiceProvider())
                {
                    int playListCount = PlayList.Count;

                    int requiredBytes = 0;
                    while (playListCount != 0)
                    {
                        playListCount >>= 8;
                        requiredBytes++;
                    }
                    byte[] randomNumberRaw = new byte[requiredBytes];
                    byte[] randomNumberHolder = new byte[sizeof(int)];
                    int randomNumber;

                    do
                    {
                        rng.GetBytes(randomNumberRaw);

                        Buffer.BlockCopy(randomNumberRaw, 0, randomNumberHolder, 0, randomNumberRaw.Length);
                        randomNumber = BitConverter.ToInt32(randomNumberHolder, 0);
                    } while (randomNumber >= PlayList.Count);

                    dg.SelectedItem = PlayList[randomNumber];
                }
            }
            else
            {
                using (var rng = new RNGCryptoServiceProvider())
                {
                    if (PlayList.All(x => x.HasPlayed))
                        foreach (var audio in PlayList)
                            audio.HasPlayed = false;

                    int playListCount = PlayList.Count;

                    int requiredBytes = 0;
                    while (playListCount != 0)
                    {
                        playListCount >>= 8;
                        requiredBytes++;
                    }
                    byte[] randomNumberRaw = new byte[requiredBytes];
                    byte[] randomNumberHolder = new byte[sizeof(int)];
                    int randomNumber;

                    do
                    {
                        rng.GetBytes(randomNumberRaw);

                        Buffer.BlockCopy(randomNumberRaw, 0, randomNumberHolder, 0, randomNumberRaw.Length);
                        randomNumber = BitConverter.ToInt32(randomNumberHolder, 0);
                    } while (randomNumber >= PlayList.Count || PlayList[randomNumber].HasPlayed);

                    dg.SelectedItem = PlayList[randomNumber];
                }
            }

            PlaySong();
        }

        private void PlayPreviousSong_Executed(object sender, EventArgs e)
        {
            if (PlayerStatus.IsPaused)
                StopSong_Executed(null, null);

            var dg = VisualTreeHelpers.FindChild<DataGrid>(this);

            for (int i = 0; i < dg.Items.Count; i++)
                if ((dg.Items[i] as Audio).Filename == playedList[playedList.Count - 2])
                {
                    dg.SelectedItem = dg.Items[i];
                    if (playedList.Count > 2)
                        playedList.RemoveRange(playedList.Count - 2, 2);
                    else
                        playedList.RemoveAt(playedList.Count - 1);
                    break;
                }

            PlaySong_Executed(null, null);
        }

        private void PlaySong()
        {
            if (player != null && (PlayerStatus.IsPlaying || PlayerStatus.IsPaused))
                StopSong_Executed(null, null);

            PlaySong_Executed(null, null);
        }

        private void PlaySong_Executed(object sender, EventArgs e)
        {
            var dg = VisualTreeHelpers.FindChild<DataGrid>(this);

            if (player != null && PlayerStatus.IsPlaying)
                PlaySong();
            else if (player != null && PlayerStatus.IsPaused)
            {
                TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Normal;

                timer.IsEnabled = true;
                player.Play();

                PlayerStatus.IsPlaying = true;
                PlayerStatus.IsPaused = false;
                PlayerStatus.IsStopped = false;
            }
            else if (dg.SelectedItem != null)
            {
                dg.ScrollIntoView(dg.SelectedItem);

                var audio = (dg.SelectedItem as Audio);

                PlaySpecificSong(audio);
            }
            else if (dg.SelectedItem == null)
                PlayNextSong_Executed(null, null);
        }

        private void PlaySpecificSong(Audio audio)
        {
            if (!LoadSong(audio))
            {
                audio.IsValid = false;

                return;
            }

            timer.Start();
            player.Play();

            PlayerStatus.CurrentSong = audio;
            TaskbarItemInfo.ProgressValue = 0;
            TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Normal;
            timer.IsEnabled = true;
            PlayerStatus.IsPlaying = true;
            PlayerStatus.IsPaused = false;
            PlayerStatus.IsStopped = false;

            string audioTitle = Path.GetFileNameWithoutExtension(audio.Filename);

            Title = $"{audioTitle}{Environment.NewLine} - {Properties.Resources.AppName}";
            
            SetAlbumArt();

            if (!string.IsNullOrEmpty(App.SPSettings.LastFMUser) && audio.Duration.TotalSeconds > 30)
                LastFM.Scrobble(PlayerStatus.CurrentSong);

            if (!string.IsNullOrEmpty(App.SPSettings.DiscordUser) && audio.Duration.TotalSeconds > 30)
                Discord.UpdateStatus(audioTitle, (int)PlayerStatus.CurrentSong.Duration.Subtract(PlayerStatus.CurrentSongTime).TotalSeconds);
        }

        private void SavePlaylist_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var sfd = new SaveFileDialog();
            sfd.AddExtension = true;
            sfd.CheckFileExists = false;
            sfd.CheckPathExists = true;
            sfd.CreatePrompt = false;
            sfd.DefaultExt = ".sp";
            sfd.Filter = Properties.Resources.SupportedPlaylistFormats;
            sfd.InitialDirectory = App.SPSettings.LastPath;
            sfd.OverwritePrompt = true;

            if (sfd.ShowDialog().Value)
            {
                File.WriteAllText(sfd.FileName, string.Empty, Encoding.UTF8);

                foreach (var audio in PreSearchPlayList)
                    File.AppendAllText(sfd.FileName, audio.Filename + Environment.NewLine);
            }
        }

        private void SetAlbumArt()
        {
            TagLib.File file = null;

            if (PlayerStatus.CurrentSong != null)
            {
                file = TagLib.File.Create(PlayerStatus.CurrentSong.Filename);

                LoadTagData(file);
            }

            var bi = new BitmapImage();
            bi.BeginInit();

            if (file != null && file.Tag.Pictures.Length > 0)
            {
                byte[] rawImage = file.Tag.Pictures[0].Data.Data;
                
                bi.StreamSource = new MemoryStream(rawImage);
            }
            else
                bi.UriSource = new Uri("pack://application:,,,/Resources/Icon.ico");

            try
            {
                bi.EndInit();
            }
            catch (Exception e) when (e is FileFormatException || e is NotSupportedException)
            {
                bi = new BitmapImage();

                bi.BeginInit();

                bi.UriSource = new Uri("pack://application:,,,/Resources/Icon.ico");

                bi.EndInit();
            }

            PlayerStatus.Image = bi;
        }

        private void Settings_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var sw = new SettingsWindow();
            sw.Owner = this;
            sw.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            sw.ShowInTaskbar = false;
            sw.ShowDialog();
        }

        private void StopSong_Executed(object sender, EventArgs e)
        {
            player.Stop();
            player.Close();

            timer.IsEnabled = false;

            PlayerStatus.CurrentSongTime = TimeSpan.Zero;

            PlayerStatus.IsPlaying = false;
            PlayerStatus.IsPaused = false;
            PlayerStatus.IsStopped = true;

            PlayerStatus.CurrentSong = null;

            Title = Properties.Resources.AppName;

            TaskbarItemInfo.ProgressState = TaskbarItemProgressState.None;

            timer.Stop();

            if (!string.IsNullOrEmpty(App.SPSettings.DiscordUser))
                Discord.ClearStatus();
        }

        private void SubtractAllAudio_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (player != null && (PlayerStatus.IsPlaying || PlayerStatus.IsPaused))
                StopSong_Executed(null, null);

            PlayList.Clear();

            PreSearchPlayList.Clear();

            WriteDefaultPlaylist();

            playedList.Clear();

            App.SPSettings.LastPlayed = string.Empty;
        }

        private void SubtractAudio_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dg = VisualTreeHelpers.FindChild<DataGrid>(this);

            if (dg.SelectedItems == null)
                return;

            for (int i = dg.SelectedItems.Count - 1; i >= 0; i--)
            {
                if (PlayerStatus.IsPlaying && PlayerStatus.CurrentSong != null && PlayerStatus.CurrentSong.Filename == (dg.Items[dg.SelectedIndex] as Audio).Filename)
                    StopSong_Executed(null, null);

                playedList.Remove((dg.Items[dg.SelectedIndex] as Audio).Filename);

                PlayList.RemoveAt(dg.SelectedIndex);
            }

            if (string.IsNullOrWhiteSpace(searchTextBox.Text))
            {
                PreSearchPlayList = PlayList.Copy();

                WriteDefaultPlaylist();

                App.SPSettings.LastPlayed = string.Empty;
            }
        }

        private void WriteDefaultPlaylist()
        {
            if (string.IsNullOrWhiteSpace(searchTextBox.Text))
                PreSearchPlayList = PlayList.Copy();
            else
                PreSearchPlayList = new ObservableCollection<Audio>(PreSearchPlayList.Union(PlayList));

            if (PlayList.Count == PreSearchPlayList.Count)
                PreSearchPlayList = PlayList.Copy();
            else
            {
                var validAudios = from audio in PlayList
                                  where audio.IsValid
                                  select audio;

                foreach (Audio audio in validAudios)
                    PreSearchPlayList[PreSearchPlayList.IndexOf(audio)] = audio;
            }

            if (PreSearchPlayList.Count == 0 && File.Exists(playlistFile))
            {
                File.Delete(playlistFile);

                return;
            }

            using (var fs = File.Open(playlistFile, FileMode.Create, FileAccess.Write))
            using (var bw = new BinaryWriter(fs))
            {
                Audio[] sortedAudios = PreSearchPlayList.OrderBy(x => x.Filename).ToArray();

                bw.Write(sortedAudios.Length);

                for (int i = 0; i < sortedAudios.Length; i++)
                {
                    bw.Write(sortedAudios[i].Duration.TotalMilliseconds);
                    bw.Write(sortedAudios[i].Filename);
                    bw.Write(sortedAudios[i].HasPlayed);
                }
            }
        }

        #endregion Executed

        #region Events

        internal void AddAudio(string toInsert)
        {
            if (player != null && (PlayerStatus.IsPlaying || PlayerStatus.IsPaused))
                StopSong_Executed(null, null);

            PlayList.Clear();

            AddDirectoryToPlayList(toInsert);

            AddFilesToPlayList(new string[] { toInsert });

            AddFilesToPlayList(File.ReadAllLines(toInsert));

            PreSearchPlayList = string.IsNullOrWhiteSpace(searchTextBox.Text) ? PlayList.Copy() : new ObservableCollection<Audio>(PreSearchPlayList.Union(PlayList));

            App.SPSettings.LastPath = File.Exists(toInsert) ? Path.GetDirectoryName(toInsert)?.Replace(".aac", "") : toInsert.Replace(Properties.Resources.AddFolder, "").Replace(".aac", "");

            App.WriteSettings(this);

            WriteDefaultPlaylist();

            if (PlayList.Count > 0)
            {
                var dg = VisualTreeHelpers.FindChild<DataGrid>(this);

                dg.SelectedItem = PlayList[0];

                Activate();

                PlaySong_Executed(null, null);
            }
            else
                PlayList.CollectionChanged += AutoplaySong;
        }

        [DllImport("user32.dll")]
        private static extern bool GetCursorPos(out POINT lpPoint);

        private static Point GetCursorPosition()
        {
            GetCursorPos(out POINT lpPoint);

            return lpPoint;
        }

        private void AutoplaySong(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            PlaySong_Executed(null, null);

            PlayList.CollectionChanged -= AutoplaySong;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e, string starterSong)
        {
            Height = App.SettingsFile.ReadSetting("Window", "Height", Height);
            Left = App.SettingsFile.ReadSetting("Window", "Left", Left);
            WindowState = App.SettingsFile.ReadSetting("Window", "Maximized", false) ? WindowState.Maximized : WindowState.Normal;
            Top = App.SettingsFile.ReadSetting("Window", "Top", Top);
            Width = App.SettingsFile.ReadSetting("Window", "Width", Width);

            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            source.AddHook(WndProc);

            if (File.Exists(playlistFile))
            {
                using (FileStream fs = File.OpenRead(playlistFile))
                using (var br = new BinaryReader(fs))
                {
                    int count = br.ReadInt32();

                    PlayerStatus.TotalDuration = TimeSpan.Zero;

                    for (int i = 0; i < count; i++)
                    {
                        var audio = new Audio();
                        audio.Duration = TimeSpan.FromMilliseconds(br.ReadDouble());
                        audio.Filename = br.ReadString();
                        audio.HasPlayed = br.ReadBoolean();
                        audio.IsValid = true;

                        PlayerStatus.TotalDuration += audio.Duration;

                        if (!File.Exists(audio.Filename))
                            continue;

                        PlayList.Add(audio);

                        if (audio.Filename == App.SPSettings.LastPlayed)
                        {
                            var dg = VisualTreeHelpers.FindChild<DataGrid>(this);

                            dg.SelectedItem = audio;

                            dg.ScrollIntoView(audio);
                        }
                    }

                    PreSearchPlayList = PlayList.Copy();
                }
            }

            if (!string.IsNullOrEmpty(starterSong))
            {
                AddAudio(starterSong);

                PlayList.CollectionChanged += AutoplaySong;
            }
        }

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point cursorPos = GetCursorPosition();
            Point windowPos = new Point(Left, Top);
            offset = (Point)(cursorPos - windowPos);

            if (Height - offset.Y < 5 || Height - offset.Y > (Height - 5) || Width - offset.X < 5 || Width - offset.X > (Width - 5))
                return;

            // capturing the mouse here will redirect all events to this window, even if the mouse cursor should leave the window area
            Mouse.Capture(this, CaptureMode.Element);
        }

        private void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Mouse.Capture(null);
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.Captured == this && Mouse.LeftButton == MouseButtonState.Pressed)
            {
                Point cursorPos = PointToScreen(Mouse.GetPosition(this));
                double newLeft = cursorPos.X - offset.X;
                double newTop = cursorPos.Y - offset.Y;

                int snappingMargin = 25;

                Screen currentScreen = Screen.PrimaryScreen;
                foreach (Screen screen in Screen.AllScreens)
                    if (newLeft > screen.WorkingArea.Left && newLeft < screen.WorkingArea.Right)
                    {
                        currentScreen = screen;
                        break;
                    }

                if (Math.Abs(currentScreen.WorkingArea.Left - newLeft) < snappingMargin)
                    newLeft = currentScreen.WorkingArea.Left;
                else if (Math.Abs(newLeft + ActualWidth - currentScreen.WorkingArea.Left - currentScreen.WorkingArea.Width) < snappingMargin)
                    newLeft = currentScreen.WorkingArea.Left + currentScreen.WorkingArea.Width - ActualWidth;

                if (Math.Abs(currentScreen.WorkingArea.Top - newTop) < snappingMargin)
                    newTop = currentScreen.WorkingArea.Top;
                else if (Math.Abs(newTop + ActualHeight - currentScreen.WorkingArea.Top - currentScreen.WorkingArea.Height) < snappingMargin)
                    newTop = currentScreen.WorkingArea.Top + currentScreen.WorkingArea.Height - ActualHeight;

                Left = newLeft;
                Top = newTop;
            }
        }

        private void PlayList_Drop(object sender, DragEventArgs e)
        {
            string[] entriesDropped = e.Data.GetData(DataFormats.FileDrop) as string[];

            foreach (var entryDropped in entriesDropped)
                AddDirectoryToPlayList(entryDropped);

            AddFilesToPlayList(entriesDropped);

            if (string.IsNullOrWhiteSpace(searchTextBox.Text))
                PreSearchPlayList = PlayList.Copy();
            else
                PreSearchPlayList = new ObservableCollection<Audio>(PreSearchPlayList.Union(PlayList));

            if (File.Exists(entriesDropped[0]))
                App.SPSettings.LastPath = Path.GetDirectoryName(entriesDropped[0]).Replace(".aac", "");
            else
                App.SPSettings.LastPath = entriesDropped[0].Replace(Properties.Resources.AddFolder, "").Replace(".aac", "");

            App.WriteSettings(this);

            WriteDefaultPlaylist();
        }

        private void PlayList_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
                SubtractAudio_Executed(null, null);
        }

        private void PlayList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PlaySong();
            e.Handled = true;
        }

        private void RegisterHotKeys()
        {
            new HotKey(Key.MediaPreviousTrack, KeyModifier.None, OnHotkeyHandler);
            new HotKey(Key.MediaPlayPause, KeyModifier.None, OnHotkeyHandler);
            new HotKey(Key.MediaNextTrack, KeyModifier.None, OnHotkeyHandler);
            new HotKey(Key.MediaStop, KeyModifier.None, OnHotkeyHandler);
        }

        private void Slider_DragCompleted(object sender, RoutedEventArgs e)
        {
            double newTime = (sender as Slider).Value;

            player.Position = TimeSpan.FromSeconds(newTime);

            dragStarted = false;
        }

        private void Slider_DragStarted(object sender, RoutedEventArgs e)
        {
            dragStarted = true;
        }

        private void Slider_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Slider_DragCompleted(sender, null);
        }

        private void TextBox_Search(object sender, RoutedEventArgs e)
        {
            string search = (sender as SearchTextBox).Text.ToLower();

            if (PreSearchPlayList.Count == 0)
                PreSearchPlayList = PlayList.Copy();
            else
            {
                PlayList.Clear();

                foreach (var audio in PreSearchPlayList)
                    PlayList.Add(audio);
            }

            if (!string.IsNullOrWhiteSpace(search))
                for (int i = PlayList.Count - 1; i >= 0; i--)
                {
                    string audioFile = Path.GetFileNameWithoutExtension(PlayList[i].Filename).ToLower();

                    if (!audioFile.Contains(search))
                        PlayList.RemoveAt(i);
                }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            PlayerStatus.CurrentSongTime = PlayerStatus.CurrentSongTime.Add(timer.Interval);

            if (PlayerStatus.CurrentSongTime <= PlayerStatus.CurrentSong.Duration && !dragStarted)
                TaskbarItemInfo.ProgressValue = PlayerStatus.CurrentSongTime.TotalSeconds / PlayerStatus.CurrentSong.Duration.TotalSeconds;
            else if (PlayerStatus.CurrentSongTime >= PlayerStatus.CurrentSong.Duration)
            {
                StopSong_Executed(null, null);

                PlayNextSong_Executed(null, null);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (player != null && (PlayerStatus.IsPlaying || PlayerStatus.IsPaused))
                StopSong_Executed(null, null);

            App.WriteSettings(this);

            WriteDefaultPlaylist();
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case WM_NCLBUTTONDOWN:
                    OnMouseLeftButtonDown(this, null);
                    break;
            }

            return IntPtr.Zero;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public static implicit operator Point(POINT point)
            {
                return new Point(point.X, point.Y);
            }
        }

        #endregion Events

        #region Window States

        private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode != ResizeMode.NoResize;
        }

        private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode == ResizeMode.CanResize || ResizeMode == ResizeMode.CanResizeWithGrip;
        }

        private void OnCloseWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }

        private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }

        private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }

        #endregion Window States
    }
}
