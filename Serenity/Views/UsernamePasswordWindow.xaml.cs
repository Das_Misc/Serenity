﻿using Serenity.Functions;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Serenity.Views
{
    /// <summary>
    /// Interaction logic for UsernamePasswordWindow.xaml
    /// </summary>
    public partial class UsernamePasswordWindow : Window
    {
        public UsernamePasswordWindow()
        {
            InitializeComponent();
        }

        private void Close_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void OK_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !string.IsNullOrWhiteSpace(Username.Text) && !string.IsNullOrWhiteSpace(Password.Text);
        }

        private void OK_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string username = Username.Text;
            string password = Password.Text;

            Task.Run(() =>
            {
                string localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                string dataDirectory = Path.Combine(localAppData, Properties.Resources.AppName);

                Discord.SignIn(username, password, dataDirectory);

                Application.Current.Dispatcher.BeginInvoke((Action)delegate ()
                {
                    Close();
                });
            });
        }
    }
}
