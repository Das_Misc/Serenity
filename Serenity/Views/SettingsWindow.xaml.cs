﻿using Microsoft.Win32;
using Serenity.Functions;
using Serenity.Models;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Serenity.Views
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow()
        {
            SPSettings = App.SPSettings;

            InitializeComponent();
        }

        public SPSettings SPSettings { get; set; }

        #region CanExecute

        private void Close_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void SignInDiscord_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = string.IsNullOrEmpty(SPSettings.DiscordUser);
        }

        private void SignInLastFM_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = string.IsNullOrEmpty(SPSettings.LastFMUser);
        }

        private void SignOutDiscord_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !string.IsNullOrEmpty(SPSettings.DiscordUser);
        }

        private void SignOutLastFM_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !string.IsNullOrEmpty(SPSettings.LastFMUser);
        }

        #endregion CanExecute

        #region Executed

        private void Close_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void RegisterExtensions_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            string applicationPath = '"' + Path.GetFullPath(thisAssembly.GetName().Name) + ".exe\"";

            string[] supportedExtensions = { "aac", "flac", "m4a", "mp3", "sp", "wav", "wma" };

            foreach (string supportedExtension in supportedExtensions)
            {
                RegistryKey regKey = Registry.ClassesRoot.OpenSubKey("." + supportedExtension, true);

                if (regKey == null)
                    regKey = Registry.ClassesRoot.CreateSubKey("." + supportedExtension, true);

                regKey.SetValue("", string.Format("{0} {1} File", Properties.Resources.AppName, supportedExtension.ToUpper()));

                regKey.CreateSubKey(@"shell\open\command").SetValue("", applicationPath + " \"%1\"");

                regKey.CreateSubKey(@"DefaultIcon").SetValue("", applicationPath + ",0");
            }

            string keyToDelete = @"Classes\Applications\Serenity.exe";

            Registry.CurrentUser.DeleteSubKeyTree(keyToDelete, false);

            Registry.LocalMachine.DeleteSubKeyTree(keyToDelete, false);

            Process.Start(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Windows), @"sysnative\ie4uinit.exe"), "-ClearIconCache");

            MessageBox.Show(Properties.Resources.AssociationsRegistered, Properties.Resources.AppName, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void SignInDiscord_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var usernamePasswordWindow = new UsernamePasswordWindow();
            usernamePasswordWindow.Owner = this;
            usernamePasswordWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            usernamePasswordWindow.ShowInTaskbar = false;
            usernamePasswordWindow.ShowDialog();
        }

        private async void SignInLastFM_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            LastFM.GetRequestToken();

            if (MessageBox.Show(this, string.Format(Properties.Resources.SignInMessage, Environment.NewLine), Properties.Resources.AppName, MessageBoxButton.OKCancel,
                                MessageBoxImage.Information, MessageBoxResult.Cancel, MessageBoxOptions.None) == MessageBoxResult.OK)
            {
                await Task.Run(() =>
                {
                    try
                    {
                        LastFM.GetSession();
                    }
                    catch (WebException)
                    {
                        LastFM.GetSession();
                    }
                });

                string localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

                string dataDirectory = Path.Combine(localAppData, Properties.Resources.AppName);

                string sessionFile = Path.Combine(dataDirectory, LastFM.session["name"]);

                LastFM.SaveSession(sessionFile);

                SPSettings.LastFMUser = LastFM.session["name"];
            }
            else
                SPSettings.LastFMUser = string.Empty;

            App.WriteSettings(Owner as MainWindow);
        }

        private void SignOutDiscord_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            string dataDirectory = Path.Combine(localAppData, Properties.Resources.AppName);

            string sessionFile = Path.Combine(dataDirectory, SPSettings.DiscordUser);

            File.Delete(sessionFile);

            SPSettings.DiscordUser = string.Empty;

            App.WriteSettings(Owner as MainWindow);
        }

        private void SignOutLastFM_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            string dataDirectory = Path.Combine(localAppData, Properties.Resources.AppName);

            string sessionFile = Path.Combine(dataDirectory, LastFM.session["name"]);

            File.Delete(sessionFile);

            SPSettings.LastFMUser = string.Empty;

            App.WriteSettings(Owner as MainWindow);
        }

        #endregion Executed
    }
}
